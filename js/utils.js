var utils = {
	helper: {
		validDate: function(strDate) {
			return ((new Date(strDate)) != 'Invalid Date');
		},
		compareDate: function(date1, date2) {
			date1 = new Date(date1);
			date2 = new Date(date2);

			return (date1.getDate() === date2.getDate() && date1.getMonth() === date2.getMonth() && date1.getYear() === date2.getYear());
		}
	},
	DOM: {
		hasClass: function(elem, classNAme) {
			return (elem.className.indexOf(classNAme) > -1);
		},
		toggleClass: function (element, classNAme) {
			if (element.className.indexOf(classNAme) < 0) {
				element.className += ' '+classNAme;
			} else {
				element.className = element.className.split(classNAme).join('');
			}
		}
	},
	events: {
		load: function() {
			var loading = document.getElementsByClassName('loading')[0];
			var loaded = document.getElementsByClassName('loaded')[0];
			var loadScreen = document.getElementsByClassName("loading-content")[0];

			loaded.style.width= 0+'%';
			loading.style.width= 100+'%';

			return {
				update: function(percent) {
					percent = parseFloat(parseFloat(percent).toFixed(2));
					var loadedW = parseFloat((loaded.style.width).replace('%',''));
					var loadingW = parseFloat((loading.style.width).replace('%',''));

					loadedW = loadedW + percent;
					loadingW = loadingW - percent;


					if (loadedW >= 100) {
						loaded.style.width= '100%';
						loading.style.width= '0%';
						loadScreen.className = loadScreen.className + ' hide';
						return;
					}

					loaded.style.width= loadedW+'%';
					loading.style.width= loadingW+'%';
				},
				status: function() {
					return (loaded.style && loaded.style.width)? loaded.style.width : '0%';
				}
			};
		},
		addEvent: function(event, selector, callback) {
			var type = selector.substring(0,1);
			selector = selector.substring(1);

			if (type === '#') {
				var element = document.getElementById(selector);
				element.addEventListener(event, callback.bind(element) , false);
			} else {
				var elements = document.getElementsByClassName(selector);
				for (var i = 0, len = elements.length; i < len; i++) {
					elements[i].addEventListener(event, callback.bind(elements[i]) , false);
				}
			}
		}
	},
	ajax: {
		GET : function (url, callback) {
			var xhr;

			if(typeof XMLHttpRequest !== 'undefined') xhr = new XMLHttpRequest();
			else {
				var versions = ["Microsoft.XmlHttp", 
								"MSXML2.XmlHttp",
								"MSXML2.XmlHttp.3.0", 
								"MSXML2.XmlHttp.4.0",
								"MSXML2.XmlHttp.5.0"];

				for(var i = 0, len = versions.length; i < len; i++) {
					try {
						xhr = new ActiveXObject(versions[i]);
						break;
					}
					catch(e){}
				} // end for
			}

			xhr.onreadystatechange = onreadystatechange;

			xhr.open('GET', url, true);
			xhr.send();

			function onreadystatechange () {
				if (xhr.readyState < 4) 
					return;

				if (xhr.status < 200 && xhr.status >= 400)
					return;

				//Format response object.
				try {
					var response = JSON.parse(xhr.responseText);
					xhr.data = response.results;
				} catch(e) { xhr.data = {};	}

				callback(xhr);
			}
		}
	},
	render: function (template, data) {
		var html = template;
		for (var key in data) {
			html = html.split('$:'+key).join(data[key]);
		}
		return html;
	}
};