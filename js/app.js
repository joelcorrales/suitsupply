document.addEventListener("DOMContentLoaded", onDocReady);

function onDocReady() {
	var loadProcess = utils.events.load();
	utils.ajax.GET('data/data.json', function dataCallback(xhr) {
		var list = document.getElementById('list');
		loadProcess.update(10);
		var template = templates.listElement();

		for (var i = 0, len = xhr.data.length; i < len; i++) {
			var data = xhr.data[i];
			data.displayImage = data.image.url;
			var render = utils.render(template, data);
			list.innerHTML += render;

			if (data.relatedStories && data.relatedStories.length > 0) {

				var principal = document.getElementsByClassName('related');
				principal = principal[i];
				principal.className += ' display';

				var relatedList = document.createElement('ul');

				for (var j = 0, l = data.relatedStories.length; j < l; j++) {
					var renderRelatedTemplate = templates.related();
					var renderRelated = utils.render(renderRelatedTemplate, data.relatedStories[j]);
					relatedList.innerHTML += renderRelated;
				}

				principal.innerHTML += relatedList.outerHTML;

			}
		}

		var images = document.getElementsByTagName("img");

		for (var i = 0, len = images.length; i < len; i++) {
			images[i].onload = function() {
				loadProcess.update(90/len);
			};
		}
		var allList = document.getElementsByClassName('list-element');
		var visibleCount = allList.length;

		utils.events.addEvent('keyup', '#filter', function(e) {
			var search = this.value;
			for (var i = 0, len = xhr.data.length; i < len; i++) {
				var filterResult = (xhr.data[i].titleNoFormatting.indexOf(search) > -1 || xhr.data[i].content.indexOf(search) > -1 );
				
				if (utils.helper.validDate(search)) {
					filterResult = utils.helper.compareDate(search, xhr.data[i].publishedDate);
				}

				if ( filterResult ) {
					if (utils.DOM.hasClass(allList[i], 'hide')) {
						visibleCount++;
						allList[i].className = allList[i].className.split('hide').join('');
					}
				} else {
					if (!utils.DOM.hasClass(allList[i], 'hide')) {
						visibleCount--;
						allList[i].className = allList[i].className + ' hide';
					}
				}
			}

			var noResults = document.getElementById('no-results');

			if (visibleCount === 0) {
				noResults.className = noResults.className.split('hide').join('');
			} else {
				noResults.className = noResults.className + ' hide';
			}
		});

		utils.events.addEvent('click', '.list-element', function(e) {
			e.preventDefault();
			e.stopPropagation();
			utils.DOM.toggleClass(this, 'open');
		});

		utils.events.addEvent('click', '.read-more', function(e) {
			e.stopPropagation();
		});

		utils.events.addEvent('click', '.related-item', function(e) {
			e.stopPropagation();
		});
	});
};