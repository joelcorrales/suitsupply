var templates = {
	listElement: function() {
		return '<li class="list-element">'+
			'<div class="image"><img src="$:displayImage"/></div>'+
			'<div class="title">$:title<br><span>$:publishedDate</span></div>'+
			'<div class="list-element-content">'+
				'<div align="center">'+
					'<p>$:content</p><br/>'+
					'<button><a href="$:unescapedUrl" class="read-more" target="_blank">Read More</a></button>'+
					'<div class="related" align="left">'+
						'<br/><br/><h3>Related Stories:</h3>'+
					'</div>'+
				'</div>'+
			'</div>'+
		'</li>';
	},
	related: function() {
		return '<li>'+
			'<a class="related-item" href="$:unescapedUrl" target="_blank">'+
				'<span class="title">$:titleNoFormatting</span><br/>'+
				'<span class="date">$:publishedDate</span>'+
			'</a>'+
		'</li>';
	}
};